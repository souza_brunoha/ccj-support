<?php
	$to = $_POST['email'];
	$html = $_POST['html'];
	$subject = $_POST['subject'];

	$curl_post_data=array(
		'from'    => 'Curso Cultura Jurídica <nao-responda@mail.colubris.com.br>',
		'to'      => $to,
		'subject' => $subject,
		'html'    => $html
	);

	$service_url = 'https://api.mailgun.net/v3/mail.colubris.com.br/messages';
	$curl = curl_init($service_url);
	curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	curl_setopt($curl, CURLOPT_USERPWD, "api:key-a86dc84eb62a748b7a818ca42e245c6a"); 

	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_POST, true);

	curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); 

	$curl_response = curl_exec($curl);  
	$response = json_decode($curl_response);
	curl_close($curl);

	echo  '{"status":true, "msg":"Mensagem enviada com sucesso!"}';
    
?>