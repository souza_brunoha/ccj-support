'use strict'

class Lesson {
    constructor(name, slug, order, interval = 0, product, folder) {
        this.name = name
        this.slug = slug
        this.order = order
        this.interval = interval
        this.product = product
        this.folder = folder
    }
}

module.exports = Lesson