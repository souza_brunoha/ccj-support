'use strict'

class Video {
    constructor(name, resume, url, thumb, area, teacher, isPrivate) {
        this.name = name
        this.resume = resume
        this.url = url
        this.thumb = thumb
        this.area = area
        this.teacher = teacher
        this.isPrivate = isPrivate
    }
}

module.exports = Video