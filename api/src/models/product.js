'use strict'

class Product {
    constructor(name, slug, type, area, description, workload, validate, price, banner, bannerType, video = '', teachers, folder, discount = 0, init) {
        this.name = name
        this.slug = slug
        this.type = type
        this.area = area
        this.description = description
        this.workload = workload
        this.validate = validate
        this.price = price
        this.banner = banner
        this.bannerType = bannerType
        this.video = video
        this.teachers = teachers
        this.folder = folder,
        this.discount = discount,
        this.init = init
    }
}

module.exports = Product