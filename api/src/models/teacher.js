'use strict'

class Teacher {
    constructor(name, surname, email, password, about, phone, birthday, gender, slug, picture) {
        this.name = name
        this.surname = surname
        this.email = email
        this.password = password
        this.about = about
        this.phone = phone
        this.birthday = birthday
        this.gender = gender
        this.slug = slug
    }
}

module.exports = Teacher