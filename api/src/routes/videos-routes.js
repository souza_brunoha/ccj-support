'use strict'

const express = require('express')
const router = express.Router()
const controller = require('./../controller/videos-controller')

router.get('/', controller.get)
router.get('/:id', controller.getById)
router.post('/', controller.create)
router.delete('/', controller.delete)
router.put('/', controller.update)

module.exports = router