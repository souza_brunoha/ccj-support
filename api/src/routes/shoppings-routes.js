'use strict'

const express = require('express')
const router = express.Router()
const controller = require('./../controller/shoppings-controller')

router.get('/', controller.get)
router.get('/:id', controller.getById)
router.post('/', controller.create)
router.post('/setup', controller.setup)
router.delete('/', controller.delete)
router.put('/:id', controller.changeStatus)

module.exports = router