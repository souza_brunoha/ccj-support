'use strict'

const express = require('express')
const router = express.Router()
const controller = require('./../controller/contact-controller')

router.post('/', controller.send)

module.exports = router