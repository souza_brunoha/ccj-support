'use strict'

const express = require('express')
const router = express.Router()
const controller = require('./../controller/support-controller')

router.post('/lessonVideo', controller.lessonVideo)
router.get('/lessonVideo/:id', controller.getLessonVideo)
router.delete('/lessonVideo/', controller.removeLessonVideo)

router.post('/lessonUser', controller.lessonUser)
router.delete('/lessonUser', controller.removeLessonUser)
router.delete('/lessonUserFromAll', controller.removeLessonUserFromAll)

router.post('/lessonVideoUser', controller.lessonVideoUser)
router.delete('/lessonVideoUser', controller.removeLessonVideoUser)
router.delete('/lessonVideoUserFromAll', controller.removeLessonVideoUserFromAll)

router.post('/lessonQuestion', controller.lessonQuestion)

router.post('/lessonMaterial', controller.lessonMaterial)
router.delete('/lessonMaterial', controller.removeLessonMaterial)

module.exports = router