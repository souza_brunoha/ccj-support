'use strict'

const express = require('express')
const router = express.Router()
const controller = require('./../controller/lessons-controller')

router.get('/', controller.get)
router.get('/:id', controller.getById)
router.get('/product/:id', controller.getByProduct)
router.post('/', controller.create)
router.put('/:id', controller.update)
router.delete('/', controller.delete)

module.exports = router