'use strict'

const express = require('express')
const bodyParser = require('body-parser')
const Parse = require('parse/node')

const config = require('./config')

Parse.initialize(config.applicationId, config.javascriptKey, config.masterKey)
Parse.serverURL = config.serverURL

const app = express()
const router = express.Router()

const index = require('./routes/index')
const products = require('./routes/products-routes')
const teachers = require('./routes/teachers-routes')
const lessons = require('./routes/lessons-routes')
const videos = require('./routes/videos-routes')
const shoppings = require('./routes/shoppings-routes')
const users = require('./routes/user-routes')
const contact = require('./routes/contact-routes')

const support = require('./routes/support-routes')

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

app.use('/', index)
app.use('/products', products)
app.use('/teachers', teachers)
app.use('/lessons', lessons)
app.use('/shoppings', shoppings)
app.use('/videos', videos)
app.use('/support', support)
app.use('/user', users)
app.use('/contact', contact)

module.exports = app