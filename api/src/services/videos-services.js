'use strict'

const Parse = require('parse/node')

exports.queryFactor = () => {
    let query = new Parse.Query('Video')
        query.include(['area', 'teatcher'])
        query.descending('createdAt')

    return query
}

exports.modelFactor = (model) => {
    return {
        id: model.id,
        name: model.get('name'),
        resume: model.get('resume'),
        url: model.get('url'),
        thumb: model.get('thumb'),
        isPrivate: model.get('isPrivate'),
        views: model.get('views'),
        area: {
            id: model.get('area').id,
            label: model.get('area').get('label')
        },
        teacher: {
            id: model.get('teatcher').id,
            name: model.get('teatcher').get('name'),
            surname: model.get('teatcher').get('surname')
        }
    }
}