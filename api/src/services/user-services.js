'use strict'

const Parse = require('parse/node')

exports.userFactor = (user) => {
    return {
        id: user.get('user').id,
        name: user.get('user').get('name'),
        surname: user.get('user').get('surname'),
        email: user.get('user').get('username')
    }
}