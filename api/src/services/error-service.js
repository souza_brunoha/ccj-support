'use strict'

exports.errorFactor = (message, code, results) => {
    const error = new Error(message)
    error.code = code
    error.results = results

    return error
}