'use strict'

const Parse = require('parse/node')

exports.queryFactor = () => {
    let query = new Parse.Query('Shopping')
        query.descending('createdAt')
        query.include(['user', 'product'])

    return query
}

exports.modelFactor = (model) => {
    let user = model.get('user')
    let product = model.get('product')

    return {
        id: model.id,
        status: model.get('status'),
        paymentStatus: model.get('paymentStatus'),
        createdAt: new Date(model.createdAt).toLocaleDateString(),
        user: {
            id: user.id,
            name: user.get('name'),
            surname: user.get('surname'),
            email: user.get('username')
        },
        transactionCode: model.get('transactionCode'),
        product: {
            id: product.id,
            name: product.get('name'),
            slug: product.get('slug'),
            type: product.get('type')
        }
    }
}