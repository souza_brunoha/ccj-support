const validator = require('validator')

class Validations {
    constructor() {
        this.errors = []
    }

    isRequired(value, message) {
        if (!value || value.length <= 0) this.errors.push({ message })
    }

    hasMinLen(value, min, message) {
        if (!value || value.length < min) this.errors.push({ message })
    }

    hasMaxLen(value, max, message) {
        if (!value || value.length > max) this.errors.push({ message })
    }

    isFixedLen(value, len, message) {
        if (!value || value.length != len) this.errors.push({ messsage })
    }

    isEmail(value, message) {
        let reg = new RegExp(/^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/)
        
        if (!reg.test(value)) this.errors.push({ message })
    }
    
    isBoolean(value, message) {
        if (value !== true && value !== false) this.errors.push({ message })
    }

    isArray(value, message) {
        if ((typeof value !== typeof new Array())) this.errors.push({ message })
    }

    isNumber(value, message) {
        if (typeof(value) !== typeof(123)) this.errors.push({ message })
    }

    validProductType(value, message) {
        if (value !== 'ISOLADA' && value !== 'COACHING' && value !== 'PREMIUM' && value !== 'POSGRADUACAO')
            this.errors.push({ message })
    }

    validProductBannerType(value, message) {
        if (value !== 'IMG' && value !== 'VIDEO') this.errors.push({ message })
    }

    validateGender(value, message) {
        if (value !== 'M' && value !== 'F') this.errors.push({ message })
    }

    validateDate(value, message) {
        if (!validator.toDate(value)) this.errors.push({ message })
    }

    validateShoppingStatus(value, message) {
        if (value !== 'PENDENTE' && value !== 'LIBERADO', value !== 'ENCERRADO') this.errors.push({ message })
    }
    
    isBase64(value, message) {
        if (!validator.isBase64(value)) this.errors.push({ message })
    }

    clear() {
        this.errors = []
    }

    isValid() {
        return this.errors.length === 0
    }
}

module.exports = Validations