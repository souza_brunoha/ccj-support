'use strict'

const Validations = require('./../helper/validations')
const repository = require('./../repositories/videos-repository')

exports.get = async (req, res, next) => {
    try {
        let videos = await repository.get()
    
        res.status(200).send({
            status: true,
            results: videos
        })        
    } catch (error) {
        res.status(400).send({
            status: false,
            message: error.message,
            code: error.code
        })        
    }
}

exports.getById = async (req, res, next) => {
    try {
        let video = await repository.getById(req.params.id)

        res.status(200).send({
            status: true,
            result: video
        })
    } catch (error) {
        res.status(400).send({
            status: false,
            message: error.message,
            code: error.code
        })
    }
}

exports.create = async (req, res, next) => {
    try {
        let body = req.body
    
        let contract = new Validations()
            contract.isRequired(body.name, 'Campo name é obrigatório')
            contract.isRequired(body.resume, 'Campo resume é obrigatório')
            contract.isRequired(body.url, 'Campo url é obrigatório')
            contract.isRequired(body.thumb, 'Campo thumb é obritagório')
            contract.isRequired(body.teacher, 'Campo teacher é obrigatório')
    
            contract.isBoolean(body.isPrivate, 'Campo isPrivate deve ser true ou false')
    
        if (!contract.isValid()) {
            res.status(400).send({
                status: false,
                errors: contract.errors
            }).end()
        
            return
        }
    
        let created = await repository.create(body)
    
        res.status(201).send({
            status: true,
            message: 'Vídeo criado com sucesso',
            video: created
        })        
    } catch (error) {
        res.status(400).send({
            status: false,
            message: error.message,
            code: error.code
        })        
    }    
}

exports.update = async (req, res, next) => {
    try {
        let updated = await repository.update(req.params.id, req.body)
    
        res.status(201).send({
            status: true,
            message: 'Vídeo modificado com sucesso',
            video: updated
        })     
    } catch (error) {
        res.status(400).send({
            status: false,
            message: error.message,
            code: error.code
        })
    }
}

exports.delete = async (req, res, next) => {
    try {
        let deleted = await repository.delete(req.body.id)
    
        res.status(201).send({
            status: true,
            message: 'Vídeo removido com sucesso',
            video: deleted
        })
    } catch (error) {
        res.status(400).send({
            status: false,
            message: error.message,
            code: error.code
        })        
    }
}