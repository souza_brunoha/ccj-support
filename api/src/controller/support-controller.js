'use strict'

const Validations = require('./../helper/validations')
const repository = require('./../repositories/support-repository')

exports.lessonMaterial = async (req, res, next) => {
    try {
        let body = req.body

        let contract = new Validations()
            contract.isRequired(body.idLesson, 'Campo idLesson é obrigatório')
            contract.isRequired(body.urlMaterial, 'Campo urlMaterial é obrigatório')
    
        if (!contract.isValid()) {
            res.status(400).send({
                status: false,
                errors: contract.errors
            }).end()
        
            return
        }

        let created = await repository.lessonMaterial(body.idLesson, body.urlMaterial)
        
        res.status(201).send({
            status: true,
            message: 'Elemento LessonMaterial criado com sucesso',
            results: created
        })

    } catch (error) {
        res.status(400).send({
            status: false,
            message: error.message === 'EXISTS' ? 'Já existe um LessonMaterial com essas informacoes' : error.message,
            code: error.code
        })
    }
}

exports.removeLessonMaterial = async (req, res, next) => {
    try {
        let body = req.body

        let contract = new Validations()
            contract.isRequired(body.idLesson, 'Campo idLesson é obrigatório')
    
        if (!contract.isValid()) {
            res.status(400).send({
                status: false,
                errors: contract.errors
            }).end()
        
            return
        }

        const removed = await repository.removeLessonMaterial(body.idLesson)
        res.status(200).send({ status: true, message: 'Materiais removidos da aula', results: removed})
        
    } catch (error) {
        res.status(400).send({
            status: false,
            message: error.message,
            code: error.code
        })              
    }
}

exports.lessonQuestion = async (req, res, next) => {
    try {
        let body = req.body
    
        let contract = new Validations()
            contract.isRequired(body.idLesson, 'Campo idLesson é obrigatório')
            contract.isRequired(body.questions, 'Campo questions é obrigatório')
    
            contract.isArray(body.questions, 'Campo questions deve ser um array')
    
            body.questions.forEach((element, i) => {
                contract.isRequired(element, `Campo questions[${i}] é obrigatório`)
            });
    
        if (!contract.isValid()) {
            res.status(400).send({
                status: false,
                errors: contract.errors
            }).end()
        
            return
        }
    
        let created = await repository.lessonQuestion(body.idLesson, body.questions)
        
        res.status(201).send({
            status: true,
            message: 'Elementos LessonQuestion criados com sucesso',
            results: created
        })        
    } catch (error) {
        res.status(400).send({
            status: false,
            message: error.message,
            code: error.code
        })        
    }
}

exports.removeLessonVideoUser = async (req, res, next) => {
    try {
        const body = req.body

        const contract = new Validations()
            contract.isRequired(body.idUser, 'Campo idUser é obrigatório')
            contract.isRequired(body.idLesson, 'Campo idLesson é obrigatório')
    
        if (!contract.isValid()) {
            res.status(400).send({
                status: false,
                errors: contract.errors
            }).end()
        
            return
        }

        const removed = await repository.removeLessonVideoUser(body.idUser, body.idLesson)
        console.log(removed)

        res.status(200).send({
            status: true,
            message: 'Elementos LessonVideoUser removidos com sucesso',
            results: removed
        })

    } catch (error) {
        res.status(400).send({
            status: false,
            message: error.message,
            code: error.code
        })        
    }
}

exports.lessonVideoUser = async (req, res, next) => {
    try {
        let body = req.body

        let contract = new Validations()
            contract.isRequired(body.idUser, 'Campo idUser é obrigatório')
            contract.isRequired(body.idLesson, 'Campo idLesson é obrigatório')
    
        if (!contract.isValid()) {
            res.status(400).send({
                status: false,
                errors: contract.errors
            }).end()
        
            return
        }

        let created = await repository.lessonVideoUser(body.idUser, body.idLesson)
        
        res.status(201).send({
            status: true,
            message: 'Elementos LessonVideoUser criados com sucesso',
            results: created
        })

    } catch (error) {
        res.status(400).send({
            status: false,
            message: error.message,
            code: error.code
        })
    }
}

exports.lessonUser = async (req, res, next) => {
    try {
        let body = req.body

        let contract = new Validations()
            contract.isRequired(body.idUser, 'Campo idUser é obrigatório')
            contract.isRequired(body.idProduct, 'Campo idProduct é obrigatório')
    
        if (!contract.isValid()) {
            res.status(400).send({
                status: false,
                errors: contract.errors
            }).end()
        
            return
        }

        let created = await repository.lessonUser(body.idUser, body.idProduct)

        if (created) {
            res.status(201).send({ status: true, message: 'Elementos LessonUser criados com sucesso', results: created})
        } else {
            res.status(200).send({ status: true, message: 'Aulas já sincronizadas com esse usuário' })
        }
        
    } catch (error) {
        res.status(400).send({
            status: false,
            message: error.message,
            code: error.code
        })        
    }
}

exports.removeLessonUser = async (req, res, next) => {
    try {
        let body = req.body

        let contract = new Validations()
            contract.isRequired(body.idUser, 'Campo idUser é obrigatório')
            contract.isRequired(body.idProduct, 'Campo idProduct é obrigatório')
            contract.isRequired(body.idLesson, 'Campo idLesson é obrigatório')
    
        if (!contract.isValid()) {
            res.status(400).send({
                status: false,
                errors: contract.errors
            }).end()
        
            return
        }

        const removed = await repository.removeLessonUser(body.idUser, body.idProduct, body.idLesson)
        res.status(200).send({ status: true, message: 'Elemento LessonUser removido com sucesso', results: removed})
        
    } catch (error) {
        res.status(400).send({
            status: false,
            message: error.message,
            code: error.code
        })          
    }
}

exports.removeLessonVideoUserFromAll = async (req, res, next) => {
    try {
        let body = req.body

        let contract = new Validations()
            contract.isRequired(body.idLesson, 'Campo idLesson é obrigatório')
    
        if (!contract.isValid()) {
            res.status(400).send({
                status: false,
                errors: contract.errors
            }).end()
        
            return
        }

        const removed = await repository.removeLessonVideoUserFromAll(body.idLesson)
        res.status(200).send({ 
            status: true, 
            message: 'Vídeos removidos de todos os usuários', 
            users: removed.users.length, 
            results: removed
        })

    } catch (error) {
        res.status(400).send({
            status: false,
            message: error.message,
            code: error.code
        })           
    }
}

exports.removeLessonUserFromAll = async (req, res, next) => {
    try {
        let body = req.body

        let contract = new Validations()
            contract.isRequired(body.idLesson, 'Campo idLesson é obrigatório')
    
        if (!contract.isValid()) {
            res.status(400).send({
                status: false,
                errors: contract.errors
            }).end()
        
            return
        }

        const removed = await repository.removeLessonUserFromAll(body.idLesson)
        res.status(200).send({ 
            status: true, 
            message: 'Aula removida de todos os usuários', 
            users: removed.users.length, 
            results: removed
        })
        
    } catch (error) {
        res.status(400).send({
            status: false,
            message: error.message,
            code: error.code
        })        
    }
}

/* 
    {
        lesson: String,
        videos: [{ video: String,  order: Int}>]
    }
*/
exports.lessonVideo = async (req, res, next) => {
    try {
        let body = req.body
    
        let contract = new Validations()
            contract.isRequired(body.lesson, 'Campo lesson é obrigatório')
            contract.isRequired(body.videos, 'Campo videos é obrigatório')
    
            contract.isArray(body.videos, 'Campo videos deve ser um array')
    
            body.videos.forEach((element, i) => {
                contract.isRequired(element.video, `Campo videos[${i}].video é obrigatório`)
                contract.isRequired(element.order, `Campo videos[${i}].order é obrigatório`)
    
                contract.isNumber(element.order, `Campo videos[${i}].order deve ser um número e maior que 0`)
            });
    
        if (!contract.isValid()) {
            res.status(400).send({
                status: false,
                errors: contract.errors
            }).end()
        
            return
        }
    
        let created = await repository.lessonVideo(body.lesson, body.videos)
        
        res.status(201).send({
            status: true,
            message: 'Elementos LessonVideo criados com sucesso',
            results: created
        })        
    } catch (error) {
        res.status(400).send({
            status: false,
            message: error.message,
            code: error.code
        })        
    }
}

exports.getLessonVideo = async (req, res, next) => {
    try {
        let id = req.params.id

        if (!id) {
            res.status(400).send({
                status: false,
                error: 'Parâmetro ID é obrigatório'
            })
        }

        let lessonVideos = await repository.getLessonVideo(id)
        res.status(200).send({ status: true, lessonVideos })

    } catch (error) {
        res.status(400).send({
            status: false,
            message: error.message,
            code: error.code
        })           
    }
}

exports.removeLessonVideo = async (req, res, next) => {
    try {
        let body = req.body

        let contract = new Validations()
            contract.isRequired(body.idLesson, 'Campo idLesson é obrigatório')
    
        if (!contract.isValid()) {
            res.status(400).send({
                status: false,
                errors: contract.errors
            }).end()
        
            return
        }

        const removed = await repository.removeLessonVideo(body.idLesson)
        res.status(200).send({ status: true, message: 'Vídeos removidos da aula', results: removed})
        
    } catch (error) {
        res.status(400).send({
            status: false,
            message: error.message,
            code: error.code
        })               
    }
}