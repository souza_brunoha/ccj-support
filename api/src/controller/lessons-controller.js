'use strict'

const Validations = require('./../helper/validations')
const repository = require('./../repositories/lesson-repository')

exports.get = async (req, res, next) => {
    try {

        let lessons = await repository.get()
        res.status(200).send({ status: true, results: lessons })

    } catch (error) {
        res.status(400).send({
            status: false,
            message: error.message,
            code: error.code
        })        
    }
}

exports.getByProduct = async (req, res, next) => {
    try {
        let lessons = await repository.getByProduct(req.params.id)

        res.status(200).send({ status: true, results: lessons })

    } catch (error) {
        res.status(400).send({
            status: false,
            message: error.message,
            code: error.code
        })
    }
}

exports.getById = async (req, res, next) => {
    try {
        let lesson = await repository.getById(req.params.id)

        res.status(200).send({ status: true, lesson })

    } catch (error) {
        res.status(400).send({
            status: false,
            message: error.message,
            code: error.code
        })
    }
}

exports.create = async (req, res, next) => {
    try {
        let body = req.body

        let contract = new Validations()
            contract.isRequired(body.name, 'O campo name é obrigatório')
            contract.isRequired(body.slug, 'O campo slug é obrigatório')
            contract.isRequired(body.folder, 'O campo folder é obrigatório')
            contract.isRequired(body.order, 'O campo order é obrigatório')
            // contract.isRequired(body.interval, 'O campo interval é obrigatório')
            contract.isRequired(body.product, 'O campo product é obrigatório')

            contract.isNumber(body.order, 'O campo order deve ser um número')
            contract.isNumber(body.interval, 'O campot interval deve ser um número')

        if (!contract.isValid()){
            res.status(400).send({
                status: false,
                errors: contract.errors
            }).end()
    
            return
        }

        let created = await repository.create(body)

        res.status(201).send({
            status: true,
            message: 'Aula criada com sucesso',
            lesson: created
        })
        
    } catch (error) {
        res.status(400).send({
            status: false,
            message: error.message,
            code: error.code
        })        
    }
}

exports.update = async (req, res, next) => {
    try {
        let updated = await repository.update(req.params.id, req.body)

        res.status(200).send({
            status: true,
            message: 'Aula editada com sucesso',
            lesson: updated
        })

    } catch (error) {
        res.status(400).send({
            status: false,
            message: error.message,
            code: error.code
        })                
    }
}

exports.delete = async (req, res, next) => {
    try {
        let deleted = await repository.delete(req.body.id)

        res.status(200).send({
            status: true,
            message: `Aula ${ deleted.name } removida com sucesso.`
        })
    } catch (error) {
        res.status(400).send({
            status: false,
            message: error.message,
            code: error.code
        })        
    }
}