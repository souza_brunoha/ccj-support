'use strict'

const Validations = require('./../helper/validations')
const repository = require('./../repositories/shopping-repository')

exports.get = async (req, res, next) => {
    try {
        let shoppings = await repository.get(req.query)

        res.status(200).send({
            status: true,
            total: shoppings.length,
            results: shoppings
        })

    } catch (error) {
        res.status(400).send({
            status: false,
            message: error.message,
            code: error.code
        })        
    }
}

exports.getById = async (req, res, next) => {
    try {
        let shopping = await repository.getById(req.params.id)

        res.status(200).send({
            status: true,
            result: shopping
        })

    } catch (error) {
        res.status(400).send({
            status: false,
            message: error.message,
            code: error.code
        })        
    }
}

exports.create = async (req, res, next) => {
    try {
        let body = req.body
        let contract = new Validations()
            contract.isRequired(body.user, 'Campo user é obrigatório')
            contract.isRequired(body.product, 'Campo product é obrigatório')

        if (!contract.isValid()) {
            res.status(400).send({
                status: false,
                errors: contract.errors
            }).end()
        
            return
        }

        let created = await repository.create(body)

        res.status(200).send({
            status: true,
            message: 'Compra criada com sucesso',
            result: created
        })

    } catch (error) {
        res.status(400).send({
            status: false,
            message: error.message,
            code: error.code
        })        
    }
}

exports.changeStatus = async (req, res, next) => {
    try {
        let body = req.body

        let contract = new Validations()
            contract.isRequired(body.status, 'Campo status é obrigatório')
            contract.validateShoppingStatus(body.status, 'Campo status deve apenas PENDENTE, LIBERADO ou ENCERRADO')

        if (!contract.isValid()) {
            res.status(400).send({
                status: false,
                errors: contract.errors
            }).end()
        
            return
        }

        let changed = await repository.changeStatus(req.params.id, body.status)

        res.status(200).send({
            status: true,
            message: 'Status modificado com sucesso',
            result: changed
        })

    } catch (error) {
        res.status(400).send({
            status: false,
            message: error.message,
            code: error.code
        })        
    }
}

exports.delete = async (req, res, next) => {
    try {
        let deleted = await repository.delete(req.body.id)

        res.status(200).send({
            status: true,
            message: 'Compra removida com sucesso'
        })

    } catch (error) {
        res.status(400).send({
            status: false,
            message: error.message,
            code: error.code
        })        
    }    
}

exports.setup = async (req, res, next) => {
    try {
        let body = req.body
        let contract = new Validations()
            contract.isRequired(body.userId, 'Campo userId é obrigatório')
            contract.isRequired(body.productId, 'Campo productId é obrigatório')

        if (!contract.isValid()) {
            res.status(400).send({
                status: false,
                errors: contract.errors
            }).end()
        
            return
        }

        let setup = await repository.setup(body)

        res.status(200).send({
            status: true,
            message: 'Compra criada com sucesso',
            result: setup
        })

    } catch (error) {
        res.status(400).send({
            status: false,
            message: error.message,
            code: error.code
        })        
    }
}