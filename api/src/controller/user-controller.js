'use strict'

const Validations = require('./../helper/validations')
const repository = require('./../repositories/user-repository')

exports.get = async (req, res, next) => {
    try {
        let email = req.query.email ? req.query.email : ''

        // let users = await repository.get(email)
        let users = await repository.get(email)

        res.status(200).send({
            status: true,
            total: users.length,
            results: users
        })

    } catch (error) {
        res.status(400).send({
            status: false,
            message: error.message,
            code: error.code
        })        
    }
}