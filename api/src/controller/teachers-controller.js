'use strict'

const Validations = require('./../helper/validations')
const repository = require('./../repositories/teachers-repository')

exports.get = async (req, res, next) => {
    try {
        let results = await repository.get()

        res.status(200).send({ status: true, results })
    } catch (error) {
        res.status(400).send({
            status: false,
            message: error.message,
            code: error.code
        })
    }
}

/*
exports.update = async (req, res, next) => {
    try {
        let body = req.body

        let contract = new Validations()

        if (body.picture) {
            contract.isBase64(body.picture, 'Campo picture deve ser um arquivo em Base64')
        }

        if (body.birthday) {
            contract.hasMinLen(body.birthday, 10, 'Campo birthday deve conter uma data no template MM-DD-YYYY')
            contract.hasMaxLen(body.birthday, 10, 'Campo birthday deve conter uma data no template MM-DD-YYYY')
            contract.validateDate(body.birthday, 'Campo birthday deve conter uma data válida')
        }

        if (body.email) {
            contract.isEmail(body.email, 'Campo email deve conter um e-mail válido')
        }

        if (body.gender) {
            contract.validateGender(body.gender, 'Campo gender deve ser M para masculino ou F para feminino')
        }

        if (body.password) {
            contract.hasMinLen(body.password, 6, 'Campo password deve conter no mínimo 6 caracteres')
        }

        if (!contract.isValid()) {
            res.status(400).send({
                status: false,
                errors: contract.errors
            }).end()
    
            return
        }

        let updated = await repository.update(req.params.id, body)

        res.status(200).send({
            status: true,
            message: 'Professor modificado com sucesso',
            teacher: updated
        })

    } catch (error) {
        res.status(400).send({
            status: false,
            message: error.code === 101 ? 'Nenhum professor encontrado' : error.message,
            code: error.code
        })        
    }
}
*/

exports.getById = async (req, res, next) => {
    try {
        let teacher = await repository.getById(req.params.id)

        res.status(200).send({ status: true, result: teacher })

    } catch (error) {
        res.status(400).send({
            status: false,
            message: error.code === 101 ? 'Nenhum professor encontrado' : error.message,
            code: error.code
        })        
    }
}

exports.delete = async (req, res, next) => {
    try {
        let deleted = await repository.delete(req.body.id)

        res.status(200).send({
            status: true,
            message: `Professor(a) ${deleted} removido com sucesso`
        })

    } catch (error) {
        res.status(400).send({
            status: false,
            message: error.code === 101 ? 'Nenhum professor encontrado' : error.message,
            code: error.code
        })
    }
}

exports.create = async (req, res, next) => {
    try {
        let body = req.body

        let contract = new Validations()
            contract.isRequired(body.name, 'Campo name é obrigatório.')
            contract.isRequired(body.surname, 'Campo surname é obrigatório.')
            contract.isRequired(body.email, 'Campo email é obrigatório.')
            contract.isRequired(body.password, 'Campo password é obrigatório.')
            contract.isRequired(body.about, 'Campo about é obrigatório.')
            contract.isRequired(body.phone, 'Campo phone é obrigatório.')
            contract.isRequired(body.birthday, 'Campo birthday é obrigatório.')
            contract.isRequired(body.gender, 'Campo gender é obrigatório.')
            contract.isRequired(body.slug, 'Campo slug é obrigatório.')

            contract.isEmail(body.email, 'Campo email deve conter um e-mail válido')

            contract.hasMinLen(body.password, 6, 'Campo password deve conter no mínimo 6 caracteres')
            contract.hasMinLen(body.birthday, 10, 'Campo birthday deve conter uma data no template MM-DD-YYYY')
            contract.hasMaxLen(body.birthday, 10, 'Campo birthday deve conter uma data no template MM-DD-YYYY')

            contract.validateGender(body.gender, 'Campo gender deve ser M para masculino ou F para feminino')
            contract.validateDate(body.birthday, 'Campo birthday deve conter uma data válida')

        if (body.picture) {
            contract.isBase64(body.picture, 'Campo picture deve ser um arquivo em Base64')
        }

        if (!contract.isValid()) {
            res.status(400).send({
                status: false,
                errors: contract.errors
            }).end()
    
            return
        } 

        let created = await repository.create(body)

        res.status(201).send({
            status: true,
            message: 'Professor criado com sucesso',
            teacher: {
                id: created.id,
                name: created.get('name'),
                surname: created.get('surname'),
                slug: created.get('slug'),
                email: created.get('email')
            }
        })

    } catch (error) {
        res.status(400).send({
            status: false,
            message: error.code === 202 ? 'Já existe um professor com esse email e username' : error.message,
            code: error.code
        })                
    }
}