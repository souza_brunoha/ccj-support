'use strict'

const Validations = require('./../helper/validations')
const repository = require('./../repositories/product-repository')

exports.delete = async (req, res, next) => {

    try {
        let product = await repository.getById(req.body.id)
        let name = product.get('name')
    
        let deleted = await repository.delete(product)
    
        res.status(200).send({
            status: true,
            message: `${name} excluído com sucesso.`
        })        
    } catch (error) {
        res.status(400).send({
            status: false,
            message: error.message
        })        
    }
}

exports.update = async (req, res, next) => {
    try {
        let product = await repository.getById(req.params.id)
        let saved = await repository.update(product, req.body)

        res.status(200).send({
            status: true,
            message: 'Produto modificado com sucesso',
            result: {
                name: saved.get('name'),
                slug: saved.get('slug'),
                id: saved.id
            }            
        })       

    } catch (error) {
        res.status(400).send({
            status: false,
            message: error.message
        })        
    }
}

exports.getById = async (req, res, next) => {
    try {
        let product = await repository.getById(req.params.id)

        res.status(200).send({
            status: true,
            result: {
                name: product.get('name'),
                slug: product.get('slug'),
                id: product.id,
                teachers: product.get('teachers').map(teacher => {
                    return {
                        name: teacher.get('name'),
                        surname: teacher.get('surname'),
                        id: teacher.id
                    }
                }),
                area: {
                    id: product.get('area').id,
                    label: product.get('area').get('label'),
                    number: product.get('area').get('number')
                }
            }
        })

    } catch (error) {
        res.status(400).send({
            status: false,
            message: error.message
        })
    }
}

exports.get = async (req, res, next) => {
    try {
        let data = await repository.get()

        res.status(200).send({
            status: true,
            results: data.map(product => {
                return {
                    name: product.get('name'),
                    slug: product.get('slug'),
                    id: product.id,
                    teachers: product.get('teachers').map(teacher => {
                        return {
                            name: teacher.get('name'),
                            surname: teacher.get('surname'),
                            id: teacher.id
                        }
                    }),
                    area: {
                        id: product.get('area').id,
                        label: product.get('area').get('label'),
                        number: product.get('area').get('number')
                    }
                }
            })
        })
    } catch (error) {
        res.status(400).send({
            status: false,
            message: error.message
        })
    }
}

exports.create = async (req, res, next) => {
    try {
        let body = req.body
        
        let contract = new Validations()
            contract.isRequired(body.name, 'Campo "name" é obrigatório')
            contract.isRequired(body.slug, 'Campo "slug" é obrigatório')
            contract.isRequired(body.type, 'Campo "type" é obrigatório')
            contract.isRequired(body.area, 'Campo "area" é obrigatório')
            contract.isRequired(body.description, 'Campo "description" é obrigatório')
            contract.isRequired(body.workload, 'Campo "workload" é obrigatório')
            contract.isRequired(body.validate, 'Campo "validate" é obrigatório')
            contract.isRequired(body.price, 'Campo "price" é obrigatório')
            contract.isRequired(body.banner, 'Campo "banner" é obrigatório')
            contract.isRequired(body.bannerType, 'Campo "bannerType" é obrigatório')
            contract.isRequired(body.teachers, 'Campo "teachers" é obrigatório')
            contract.isRequired(body.folder, 'Campo "folder" é obrigatório')
    
            contract.validProductType(body.type, 'Valor incorreto para o campo "type"')
            contract.validProductBannerType(body.bannerType, 'Valor incorreto para o campo "bannerType"')
    
            contract.isNumber(body.workload, 'O campo "workload" deve ser um número e maior que 0')
            contract.isNumber(body.validate, 'O campo "validate" deve ser um número e maior que 0')
            contract.isNumber(body.price, 'O campo "price" deve ser um número e maior que 0')
            contract.isNumber(body.discount, 'O campo discount deve ser um número maior ou igual a 0')
            contract.validateDate(body.init, 'O campo init deve conter uma data válida')
    
            contract.isArray(body.teachers, 'O campo "teachers" deve ser um array')
    
        if (!contract.isValid()){
            res.status(400).send({
                status: false,
                errors: contract.errors
            }).end()
    
            return
        }

        let productExists = await repository.getBySlug(body.slug)
    
        if (productExists) {
            res.status(400).send({
                status: false,
                message: `Já existe um produto com o slug ${body.slug}.`
            }).end()
    
            return
        }
    
        let areaExists = await repository.getArea(body.area)
        let created = await repository.create(body)
        
        res.status(201).send({
            status: true,
            message: 'Produto criado com sucesso',
            result: {
                name: created.get('name'),
                slug: created.get('slug'),
                id: created.id
            }
        })
        
    } catch (error) {
        res.status(400).send({
            status: false,
            message: error.message
        })        
    }
}