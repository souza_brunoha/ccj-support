'use strict'

const Validations = require('./../helper/validations')
const repository = require('./../repositories/contact-repository')

exports.send = async (req, res, next) => {
    try {
        const body = req.body

        const contract = new Validations()
        contract.isRequired(body.idUser, 'Campo idUser é obrigatório')
        contract.isRequired(body.text, 'Campo text é obrigatório')
        contract.isRequired(body.subject, 'Campo subject é obrigatório')
    
        if (!contract.isValid()) {
            res.status(400).send({
                status: false,
                errors: contract.errors
            }).end()
        
            return
        }

        const sent = await repository.sendEmail(body.idUser, body.text, body.subject)

        res.status(200).send({ status: true, message: 'E-mail enviado com sucesso', user: sent })
        
    } catch (error) {
        res.status(400).send({
            status: false,
            message: error.message,
            code: error.code
        })               
    }
}