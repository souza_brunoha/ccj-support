'use strict'

const Parse = require('parse/node')
const Aula = require('./../models/lesson')

let mountModel = (lesson, bool) => {

    let model = {
        id: lesson.id,
        name: lesson.get('name'),
        slug: lesson.get('slug'),
        folder: lesson.get('folder'),
        interval: lesson.get('interval'),
        order: lesson.get('order')
    }

    if (bool) {
        let product = lesson.get('product')
        model.product = {
            name: product.get('name'),
            slug: product.get('slug'),
            id: product.id            
        }
    }

    return model
}

exports.get = async () => {
    let query = new Parse.Query('Lesson')
        query.ascending('order')
        query.include('product')

    let lessons = await query.find()

    return lessons.map(lesson => mountModel(lesson, true))
}

exports.getByProduct = async (id) => {
    let query = new Parse.Query('Product')
    let product = await query.get(id)

    let innerquery = new Parse.Query('Lesson')
        innerquery.equalTo('product', product)
        innerquery.include(product)
        innerquery.ascending('order')

    let lessons = await innerquery.find()

    return {
        product: {
            name: product.get('name'),
            slug: product.get('slug'),
            id: product.id
        },
        lessons: lessons.map(lesson => mountModel(lesson, false))
    }
}

exports.getById = async (id) => {
    let query = new Parse.Query('Lesson')
        query.include('product')

    let lesson = await query.get(id)

    return mountModel(lesson, true)
}

exports.create = async (data) => {
    let query = new Parse.Query('Product')
    
    let product = await query.get(data.product)

    let model = new Aula(data.name, data.slug, data.order, data.interval, data.product, data.folder)

    let Lesson = Parse.Object.extend('Lesson')
    let lesson = new Lesson()
        lesson.set('name', model.name)
        lesson.set('slug', model.slug)
        lesson.set('folder', model.folder)
        lesson.set('order', model.order)
        lesson.set('interval', model.interval)
        lesson.set('product', product)

    let created = await lesson.save()

    return mountModel(created, true)
}

exports.update = async (id, data) => {
    let query = new Parse.Query('Lesson')
        query.include('product')
        
    let lesson =  await query.get(id)

    if (data.name) lesson.set('name', data.name)
    if (data.slug) lesson.set('slug', data.slug)
    if (data.order) lesson.set('order', data.order)
    if (data.interval) lesson.set('interval', data.interval)

    let saved = await lesson.save()

    return mountModel(lesson, true)
}

exports.delete = async (id) => {
    let query =  new Parse.Query('Lesson')

    let lesson = await query.get(id)
    let deleted = await lesson.destroy()

    return { name: mountModel(lesson).name }
}