'use strict'

const Parse = require('parse/node')
const service = require('./../services/shoppings-services')
const errors = require('./../services/error-service')
const Shop = require('./../models/shopping')

const Lesson = require('./lesson-repository')
const Support = require('./support-repository')


exports.get = async (filters) => {
    let query = service.queryFactor()

    if (filters.user) {
        let user = new Parse.User()
            user.id = filters.user

        query.equalTo('user', user)
    }

    if (filters.product) {
        let Product = Parse.Object.extend('Product')
        let product = new Product()
            product.id = filters.product

        query.equalTo('product', product)
    }

    if (filters.status) {
        query.equalTo('status', filters.status)
    }

    let shoppings = await query.find()

    // return shoppings.map(shopping => service.modelFactor(shopping))
    return shoppings.map((shopping, index) => `${index + 1}: ${shopping.get('user').id}`)
}

exports.getById = async (id) => {
    let query = service.queryFactor()
    let shopping = await query.get(id)

    return service.modelFactor(shopping)
}

exports.changeStatus = async (id, status) => {
    let query = service.queryFactor()
    let shopping = await query.get(id)
        shopping.set('status', status)

    let changed = await shopping.save()

    return service.modelFactor(changed)
}

exports.delete = async (id) => {
    let query = service.queryFactor()
    let shopping = await query.get(id)

    let deleted = await shopping.destroy()

    return service.modelFactor(deleted)
}

exports.create = async (data) => {    
    let product = await new Parse.Query('Product').get(data.product)
    let user = await new Parse.Query('User').get(data.user)

    const shopQuery = new Parse.Query('Shopping')
    shopQuery.equalTo('user', user)
    shopQuery.equalTo('product', product)
    shopQuery.notEqualTo('status', 'LIBERADO')
    shopQuery.include(['user', 'product'])

    const shops = await shopQuery.find()

    if (shops.length) {
        throw errors.errorFactor(
            'Já existe compra ativa desse curso para o usuário informado', 
            undefined, 
            shop.map(s => service.modelFactor(s))
        )
    }

    let Shopping = Parse.Object.extend('Shopping')
    let shopping = new Shopping()
        shopping.set('user', user)
        shopping.set('product', product)
        shopping.set('status', 'LIBERADO')
        shopping.set('paymentStatus', 'APROVADO')
        shopping.set('transactionCode', 'AUTO')

    let created = await shopping.save()

    return service.modelFactor(created)

}

exports.setup = async (data) => {
    const lessons = await Lesson.getByProduct(data.productId)

    const createdShop = await this.create({ product: data.productId, user: data.userId })
    const createdLessonsUser = await Support.lessonUser(data.userId, data.productId)
    const createdLessonVideosUser = []    
    
    await Promise.all(lessons.lessons.map(async (lesson) => {
        const lessonVideosUser = await Support.lessonVideoUser(data.userId, lesson.id)
        createdLessonVideosUser.push(lessonVideosUser)
    }))

    return {
        shopping: createdShop,
        lessons: createdLessonsUser,
        videos: createdLessonVideosUser
    }
}