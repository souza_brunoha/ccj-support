'use strict'

const Parse = require('parse/node')
const service = require('./../services/videos-services')
const Video = require('./../models/video')

exports.create = async (data) => {
    let model = new Video(data.name, data.resume, data.url, data.thumb, data.area, data.teacher, data.isPrivate)

    let areaQuery = new Parse.Query('Area')
    let area = await areaQuery.get(model.area)

    let teacherQuery = new Parse.Query('User')
        teacherQuery.equalTo('isTeatcher', true)
    let teacher = await teacherQuery.get(model.teacher)

    let VideoClass = Parse.Object.extend('Video')
    let video = new VideoClass()
        video.set('name', model.name)
        video.set('resume', model.resume)
        video.set('module', area.get('number'))
        video.set('teatcher', teacher)
        video.set('url', model.url)
        video.set('area', area)
        video.set('thumb', model.thumb)
        video.set('isPrivate', model.isPrivate)
        video.set('views', 0)

    let created = await video.save()

    return service.modelFactor(created)
}

exports.update = async (id, data) => {
    let query = service.queryFactor()
    let video = await query.get(id)

    if (data.area) {
        let areaQuery = new Parse.Query('Video')
        let area = await areaQuery.get(data.area)
        video.set('area', area)
        video.set('module', area.get('number'))
    }

    if (data.teacher) {
        let teacherQuery = new Parse.Query('User')
            teacherQuery.equalTo('isTeatcher', true)

        let teacher = await teacherQuery.get(data.teacher)
        video.set('teatcher', teacher)
    }

    if (data.name) video.set('name', data.name)
    if (data.resume) video.set('resume', data.resume)
    if (data.url) video.set('url', data.url)
    if (data.thumb) video.set('thumb', data.thumb)
    if (data.isPrivate) video.set('isPrivate', data.isPrivate)

    let updated = await video.save()

    return service.queryFactor(updated)
}

exports.getById = async (id) => {
    let query = service.queryFactor()
    let video = await query.get(id)

    return service.modelFactor(video)
}

exports.get = async () => {
    let query = service.queryFactor()
    let videos = await query.find()

    return videos.map(video => service.modelFactor(video))
}

exports.delete = async (id) => {
    let query = service.queryFactor()
    let video = await query.get(id)
    let response = service.modelFactor(video)
    let deleted = await video.destroy()

    return response
}