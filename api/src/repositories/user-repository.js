'use strict'

const Parse = require('parse/node')

exports.get = async (email) => {
    let query = new Parse.Query('User')
        query.ascending('username')
        query.equalTo('isTeatcher', false)

    if (email) query.equalTo('email', email)

    const users = await query.find()

    return users.map(user => {
        return {
            id: user.id,
            username: user.get('username'),
            name: user.get('name') + ' ' + user.get('surname')
        }
    })
}