'use strict'

const Parse = require('parse/node')
const services = require('./../services/user-services')

exports.removeLessonMaterial = async (idLesson) => {
    let Lesson = Parse.Object.extend('Lesson')
    let lesson = new Lesson()
        lesson.id = idLesson

    let query = new Parse.Query('LessonMaterial')
        query.equalTo('lesson', lesson)
        query.ascending('createdAt')
        query.include(['lesson'])

    let materials = await query.find()

    let removed = await Parse.Object.destroyAll(materials)

    return materials.map(material => {
        return {
            id: material.id,
            url: material.get('url'),
            lesson: {
                id: material.get('lesson').id,
                name: material.get('lesson').get('name'),
                slug: material.get('lesson').get('slug')
            }
        }
    })
}

exports.lessonMaterial = async (idLesson, url) => {
    let Lesson = Parse.Object.extend('Lesson')
    let lesson = new Lesson()
        lesson.id = idLesson

    let query = new Parse.Query('LessonMaterial')
        query.equalTo('lesson', lesson)
        query.equalTo('url', url)

    let response = await query.first()
    
    if (response) {
        return new Error('EXISTS')
    }

    let LessonMaterial = Parse.Object.extend('LessonMaterial')
    let lessonMaterial = new LessonMaterial()
        lessonMaterial.set('url', url)
        lessonMaterial.set('lesson', lesson)
    
    let created = await lessonMaterial.save()
    
    return {
        id: created.id,
        url: created.get('url'),
        lesson: {
            id: created.get('lesson').id,
            name: created.get('lesson').get('name'),
            slug: created.get('lesson').get('slug')
        }
    }
}

exports.lessonQuestion = async (idLesson, questions) => {
    let query = new Parse.Query('Lesson')

    let lesson = await query.get(idLesson)
    let lessonQuestions = []

    questions.forEach(idQuestion => {
        let Question = Parse.Object.extend('Question')
        let question = new Question()
            question.id = idQuestion

        let LessonQuestion = Parse.Object.extend('LessonQuestion')
        let lessonQuestion = new LessonQuestion()
            lessonQuestion.set('lesson', lesson)
            lessonQuestion.set('question', question)

        lessonQuestions.push(lessonQuestion)
    })

    let created = await Parse.Object.saveAll(lessonQuestions)

    return {
        lesson: {
            id: lesson.id,
            name: lesson.get('name'),
            slug: lesson.get('slug')
        },
        questions: created.map(lessonQuestion => {
            let q = lessonQuestion.get('question')

            return {
                id: q.id,
                text: q.get('text')
            }
        })
    }
}

/* exports.removeLessonVideoUser = async (idUser, idLesson) => {
    let query = new Parse.Query('User')
    let user = await query.get(idUser)

    query = new Parse.Query('Lesson')

    let lesson = await query.get(idLesson)

    query = new Parse.Query('LessonVideo')
    query.equalTo('lesson', lesson)
    query.ascending('order')
    query.include(['video'])

    let lessonVideos = await query.find()

    query = new Parse.Query('LessonVideoUser')
    query.equalTo('user', user)
    query.containedIn('lessonVideo', lessonVideos)
    query.include(['lessonVideo.video', 'lessonVideo.lesson'])

    let lessonVideosUser = await query.find()

    if (!lessonVideosUser.length) { return new Error('EMPTY') }

    return lessonVideosUser.map(lessonVideoUser => {
        let video = lessonVideoUser.get('lessonVideo').get('video')
        let lesson = lessonVideoUser.get('lessonVideo').get('lesson')

        return {
            id: lessonVideoUser.id,
            order: lessonVideoUser.get('lessonVideo').get('order'),
            video: {
                id: video.id,
                name: video.get('name')
            },
            lesson: {
                id: lesson.id,
                name: lesson.get('name'),
                order: lesson.get('order')
            }
        }
    })

} */

exports.lessonVideoUser = async (idUser, idLesson) => {
    let lessonQuery = new Parse.Query('Lesson')
        lessonQuery.include(['product'])
    let userQuery = new Parse.Query('User')

    let lesson = await lessonQuery.get(idLesson)
    let user = await userQuery.get(idUser)

    let lessonVideoQuery = new Parse.Query('LessonVideo')
        lessonVideoQuery.equalTo('lesson', lesson)
        lessonVideoQuery.ascending('order')

    let lessonVideos = await lessonVideoQuery.find()

    let lessonVideoUserQuery = new Parse.Query('LessonVideoUser')
        lessonVideoUserQuery.equalTo('user', user)
        lessonVideoUserQuery.containedIn('lessonVideo', lessonVideos)
        lessonVideoUserQuery.include(['lessonVideo.video, lessonVideo.lesson', 'user'])

    let lessonVideosUser = await lessonVideoUserQuery.find()
    
    let saveaall = []

    lessonVideos.forEach(lessonVideo => {
        let val = lessonVideosUser.find(el => el.get('lessonVideo').id === lessonVideo.id)

        if (!val) {
            let LessonVideoUser = Parse.Object.extend('LessonVideoUser')
            let lessonVideoUser = new LessonVideoUser()
                lessonVideoUser.set('user', user)
                lessonVideoUser.set('lessonVideo', lessonVideo)
                lessonVideoUser.set('watched', false)
    
            saveaall.push(lessonVideoUser)
        }
        
    })

    if (!saveaall.length) {
        return {
            message: 'Todos os vídeos já estão vinculados ao usuário'
        }
    }

    let created = await Parse.Object.saveAll(saveaall)

    return created.map(el => {
        return {
            id: el.id,
            order: el.get('lessonVideo').get('order'),
            video: {
                id: el.get('lessonVideo').get('video').id,
                name: el.get('lessonVideo').get('video').get('name')
            }
        }
    })
}

exports.lessonUser = async (idUser, idProduct) => {    
    let day = 1000 * 60 * 60 * 24        
    let arr = []
    let lastDay = new Date().getTime()

    let queryUser = new Parse.Query('User')
    let queryProduct = new Parse.Query('Product')
        queryProduct.containedIn('type', ['ISOLADA', 'COACHING'])
    
    let user = await queryUser.get(idUser)
    let product = await queryProduct.get(idProduct)

    let shoppingQuery = new Parse.Query('Shopping')
        shoppingQuery.equalTo('user', user)
        shoppingQuery.equalTo('product', product)
        shoppingQuery.equalTo('status', 'LIBERADO')
        shoppingQuery.descending('createdAt')

    let shopping = await shoppingQuery.first()

    let lessonQuery = new Parse.Query('Lesson')
        lessonQuery.equalTo('product', product)
        lessonQuery.ascending('order')

    let lessons = await lessonQuery.find()

    let lessonUserQuery = new Parse.Query('LessonUser')
        lessonUserQuery.equalTo('shopping', shopping)
        lessonUserQuery.include(['lesson'])

    let lessonsUser = await lessonUserQuery.find()

    if (lessonsUser.length === lessons.length) {
        return false
    }
    
    lessons.forEach(lesson => {
        let l = lessonsUser.find(el => el.get('lesson').id === lesson.id)

        if (!l){
            let LessonUser = Parse.Object.extend('LessonUser')
            let lessonUser = new LessonUser()
                lessonUser.set('shopping', shopping)
                lessonUser.set('lesson', lesson)
            
            if (product.get('type') === 'ISOLADA') {
                lessonUser.set('status', 'ANDAMENTO')
            }
        
            if (product.get('type') === 'COACHING') {
                if (lesson.get('order') === 1) {
                    lessonUser.set('status', 'ANDAMENTO')
                    lastDay = lastDay - day
                } else {
                    lessonUser.set('status', 'BLOQUEADO')
                }
        
                let interval = lesson.get('interval')
                
                let init = lastDay.getTime() + day
                let end = (interval * day) + init
                lastDay = end
        
                lessonsUser.set('init', new Date(init))
                lessonsUser.set('end', new Date(end))
            }
    
            arr.push(lessonUser)
        }
    })

    let created = await Parse.Object.saveAll(arr)

    return created.map(el => {
        let response = {
            id: el.id,
            lesson: {
                id: el.get('lesson').id,
                name: el.get('lesson').get('name'),
                slug: el.get('lesson').get('slug'),
                order: el.get('lesson').get('order')
            },
            status: el.get('status')
        }

        if (product.get('type') === 'COACHING') {
            response.init = el.get('init')
            response.end = el.get('end')
        }
        
        return response
    })
    
}

exports.removeLessonUser = async (idUser, idProduct, idLesson) => {
    const userQuery = new Parse.Query('User')
    const user = await userQuery.get(idUser)

    const productQuery = new Parse.Query('Product')
    const product = await productQuery.get(idProduct)

    const shopQuery = new Parse.Query('Shopping')
    shopQuery.equalTo('status', 'LIBERADO')
    shopQuery.equalTo('product', product)
    shopQuery.equalTo('user', user)
    const shopping = await shopQuery.first()

    const lessonQuery = new Parse.Query('Lesson')
    const lesson = await lessonQuery.get(idLesson)

    const lessonUserQuery = new Parse.Query('LessonUser')
    lessonUserQuery.equalTo('shopping', shopping)
    lessonUserQuery.equalTo('lesson', lesson)

    const lessonUser = await lessonUserQuery.first()

    const removed = await lessonUser.destroy()

    return {
        user: {
            id: user.id,
            name: user.get('name'),
            surname: user.get('surname'),
            email: user.get('username')
        },
        lesson: {
            id: lesson.id,
            order: lesson.get('order'),
            name: lesson.get('name'),
            slug: lesson.get('slug')
        }
    }
}

exports.getLessonVideo = async (id) => {
    let queryLesson = new Parse.Query('Lesson')
    let lesson = await queryLesson.get(id)

    let query = new Parse.Query('LessonVideo')
        query.equalTo('lesson', lesson)
        query.include(['video'])
        query.ascending('order')

    let videos = await query.find()

    let response = {
        lesson: {
            id: lesson.id,
            name: lesson.get('name'),
            slug: lesson.get('slug'),
            order: lesson.get('order')
        },
        videos: videos.map(lessonVideo => {
            return {
                video: lessonVideo.get('video').id,
                order: lessonVideo.get('order')
                // name: lessonVideo.get('video').get('resume')
            }
        })
    }

    return response

}

exports.lessonVideo = async (idLesson, videos) => {
    let queryLesson = new Parse.Query('Lesson')
    
    let lesson = await queryLesson.get(idLesson)

    let lessonVideos = []

    videos.forEach(el => {
        let Video = Parse.Object.extend('Video')        
        let video = new Video()
            video.id = el.video

        let LessonVideo = Parse.Object.extend('LessonVideo')
        let lessonVideo = new LessonVideo()
            lessonVideo.set('video', video)
            lessonVideo.set('lesson', lesson)
            lessonVideo.set('order', el.order)

        lessonVideos.push(lessonVideo)
    })

    let created = await Parse.Object.saveAll(lessonVideos)

    return created.map(lessonVideo => {
        return {
            id: lessonVideo.id,
            order: lessonVideo.get('order'),
            video: {
                id: lessonVideo.get('video').id,
                name: lessonVideo.get('video').get('name'),
                url: lessonVideo.get('video').get('url')
            },
            lesson: {
                id: lesson.id,
                name: lesson.get('name')
            }
        }
    })
}

exports.removeLessonVideo = async (idLesson) => {
    const lessonQuery = new Parse.Query('Lesson')
    const lesson = await lessonQuery.get(idLesson)

    const lessonVideoQuery = new Parse.Query('LessonVideo')
    lessonVideoQuery.equalTo('lesson', lesson)
    lessonVideoQuery.ascending('order')

    const lessonVideos = await lessonVideoQuery.find()

    const removed = await Parse.Object.destroyAll(lessonVideos)

    return {
        lesson: {
            id: lesson.id,
            name: lesson.get('name'),
            slug: lesson.get('slug'),
            order: lesson.get('order')
        },
        videos: removed.map(rem => {
            let video = rem.get('video')
            return {
                id: video.id,
                name: video.get('name'),
                order: rem.get('order')
            }
        })
    }
}

exports.removeLessonVideoUser = async (userId, lessonId) => {
    let userQuery = new Parse.Query('User')    
    let lessonQuery = new Parse.Query('Lesson')
        lessonQuery.include(['product'])
    let lessonVideoQuery = new Parse.Query('LessonVideo')
        lessonVideoQuery.ascending('order')
    let lessonVideoUserQuery = new Parse.Query('LessonVideoUser')
        lessonVideoUserQuery.include(['lessonVideo', 'lessonVideo.video'])
    
    const user = await userQuery.get(userId)
    const lesson = await lessonQuery.get(lessonId)
    
    lessonVideoQuery.equalTo('lesson', lesson)

    const lessonVideos = await lessonVideoQuery.find()

    lessonVideoUserQuery.equalTo('user', user)
    lessonVideoUserQuery.containedIn('lessonVideo', lessonVideos)

    const lessonVideosUser = await lessonVideoUserQuery.find()

    const removed = await Parse.Object.destroyAll(lessonVideosUser)

    return {
        user: {
            id: user.id,
            name: user.get('name'),
            surname: user.get('surname'),
            email: user.get('username')
        },
        lesson: {
            id: lesson.id,
            name: lesson.get('name'),
            slug: lesson.get('slug')
        },
        lessonVideos: removed.map(rem => {
            return {
                id: rem.get('lessonVideo').id,
                order: rem.get('lessonVideo').get('order'),
                name: rem.get('lessonVideo').get('video').get('name')
            }
        })
    }
}

exports.removeLessonVideoUserFromAll = async (idLesson) => {
    const lessonQuery = new Parse.Query('Lesson')
    lessonQuery.include(['product'])

    const lesson = await lessonQuery.get(idLesson)
    const product = lesson.get('product')

    const lessonVideoQuery = new Parse.Query('LessonVideo')
    lessonVideoQuery.equalTo('lesson', lesson)
    lessonVideoQuery.include(['video'])
    lessonVideoQuery.ascending('order')

    const lessonVideos = await lessonVideoQuery.find()

    const lessonVideoUserQuery = new Parse.Query('LessonVideoUser')
    lessonVideoUserQuery.containedIn('lessonVideo', lessonVideos)

    const lessonVideoUsers = await lessonVideoUserQuery.find()

    const removed = await Parse.Object.destroyAll(lessonVideoUsers)

    return {
        product: {
            id: product.id,
            name: product.get('name'),
            slug: product.get('slug')
        },
        lesson: {
            id: lesson.id,
            name: lesson.get('name'),
            slug: lesson.get('slug'),
            order: lesson.get('order'),
            videos: lessonVideos.map(el => {
                return {
                    id: el.get('video').id,
                    name: el.get('video').get('name'),
                    order: el.get('order')
                }
            })
        },
        users: [...new Set(removed.map(rem => JSON.stringify(services.userFactor(rem))))].map(user => JSON.parse(user)) //HARD CODE, MOTHAFUCKAAAAAA!
    }

}

exports.removeLessonUserFromAll = async (idLesson) => {
    const lessonQuery = new Parse.Query('Lesson')
    lessonQuery.include(['product'])

    const lesson = await lessonQuery.get(idLesson)
    const product = lesson.get('product')

    const lessonUserQuery = new Parse.Query('LessonUser')
    lessonUserQuery.equalTo('lesson', lesson)
    lessonUserQuery.include(['shopping', 'shopping.user'])

    const lessonUsers = await lessonUserQuery.find()
    const removed = await Parse.Object.destroyAll(lessonUsers)

    return {
        product: {
            id: product.id,
            name: product.get('name'),
            slug: product.get('slug')
        },
        lesson: {
            id: lesson.id,
            name: lesson.get('name'),
            slug: lesson.get('slug'),
            order: lesson.get('order')
        },
        users: removed.map(rem => {
            const user = rem.get('shopping').get('user')

            return {
                id: user.id,
                name: user.get('name'),
                surname: user.get('surname'),
                email: user.get('username')
            }
        })
    }
}