'use strict'

const Parse = require('parse/node')
const shoppingRepo = require('./shopping-repository')
const supportRepo = require('./lesson-repository')

exports.createShopping = async (user, product) => {
    const shopping = await shoppingRepo.create({ user, product })
    const lessonsUser = await supportRepo.lessonsUser(user, product)
}