'use strict'

const Parse = require('parse/node')
const Teacher = require('./../models/teacher')

/*
exports.update = async (id, data) => {
    let query = new Parse.Query('User')
        query.equalTo('isTeatcher', true)

    let genders = { 'M': 0, 'F': 1 }
    
    let teacher = await query.get(id)

    if (data.name) teacher.set('name', data.name)
    if (data.surname) teacher.set('surname', data.surname)
    if (data.email) teacher.set('username', data.email)
    if (data.email) teacher.set('email', data.email)
    if (data.password) teacher.set('password', data.password)
    if (data.about) teacher.set('about', data.about)
    if (data.birthday) teacher.set('birthday', new Date(data.birthday))
    if (data.gender) teacher.set('gender', genders[data.gender])
    if (data.slug) teacher.set('slug', data.slug)

    if (data.picture) {
        let picture = new Parse.File('photo.jpg', { base64: data.picture })
        teacher.set('picture', picture)
    }

    let saved = await teacher.save()

    return {
        id: saved.id,
        name: saved.get('name'),
        surname: saved.get('surname'),
        email: saved.get('email'),
        username: saved.get('username'),
        about: saved.get('about'),
        phone: saved.get('phone'),
        birthday: saved.get('birthday'),
        gender: data.gender,
        slug: saved.get('slug')
    }
}
*/

exports.get = async () => {
    let query = new Parse.Query('User')
        query.equalTo('isTeatcher', true)
        query.ascending('name')

    let response = await query.find()
    let genders = ['M', 'F']

    let teachers = response.map(teacher => {
        return {
            id: teacher.id,
            name: teacher.get('name'),
            surname: teacher.get('surname'),
            email: teacher.get('email'),
            username: teacher.get('username'),
            about: teacher.get('about'),
            phone: teacher.get('phone'),
            birthday: teacher.get('birthday'),
            gender: genders[teacher.get('gender')],
            slug: teacher.get('slug')
        }
    })

    return teachers
}

exports.getById = async (id) => {
    let query = new Parse.Query('User')
        query.equalTo('isTeatcher', true)
        
    let teacher = await query.get(id)
    let genders = ['M', 'F']

    return {
        id: teacher.id,
        name: teacher.get('name'),
        surname: teacher.get('surname'),
        email: teacher.get('email'),
        username: teacher.get('username'),
        about: teacher.get('about'),
        phone: teacher.get('phone'),
        birthday: teacher.get('birthday'),
        gender: genders[teacher.get('gender')],
        slug: teacher.get('slug')
    }
}

exports.delete =  async (id) => {
    let query = new Parse.Query('User')
        query.equalTo('isTeatcher', true)

    let teacher = await query.get(id)
    let name = `${teacher.get('name')} ${teacher.get('surname')}`
    
    let deleted = await teacher.destroy({ useMasterKey: true })

    return name
}

exports.create = async (data) => {
    let genders = { 'M': 0, 'F': 1}

    let model = new Teacher(data.name, data.surname, data.email, data.password, data.about, data.phone, data.birthday, genders[data.gender], data.slug, data.picture)

    let teacher = new Parse.User()
        teacher.set('name', model.name)
        teacher.set('surname', model.surname)
        teacher.set('username', model.email)
        teacher.set('email', model.email)
        teacher.set('password', model.password)
        teacher.set('about', model.about)
        teacher.set('phone', model.phone)
        teacher.set('userModules', [])
        teacher.set('birthday', new Date(model.birthday))
        teacher.set('gender', model.gender)
        teacher.set('slug', model.slug)
        teacher.set('isTeatcher', true)
        // teacher.set('emailVerified', true)
        teacher.set('receiveNewsletter', false)    

    if (model.picture) {
        let picture = new Parse.File('photo.jpg', { base64: model.picture })
        teacher.set('picture', picture)
    }

    let created = await teacher.signUp()

    return created
}