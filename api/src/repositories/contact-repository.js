'use strict'

const Parse = require('parse/node')
const querystring = require('querystring')

const mailgun = require('mailgun.js')
const domain = 'mail.colubris.com.br'
const mlgn = mailgun.client({ username: 'api', key: 'key-a86dc84eb62a748b7a818ca42e245c6a' })

exports.sendEmail = async (userId, text, subject) => {
    const query = new Parse.Query('User')
    const user = await query.get(userId)

    const to = user.get('username').toLowerCase()
    // const to = 'souza.brunoha@gmail.com'
    const from = 'Curso Cultura Jurídica <nao-responda@mail.colubris.com.br>'

    /* 
    to: 'souza.brunoha@gmail.com',
    from: 'Curso Cultura Jurídica <nao-responda@mail.colubris.com.br>',
    subject,
    html: text    
     */

    const postData = {
        from,
        to,
        subject,
        html: text
    }

    const response = await mlgn.messages.create(domain, postData)

    return { email: to, name: user.get('name'), surname: user.get('surname'), id: user.id }
}