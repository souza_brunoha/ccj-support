'use strict'

const Parse = require('parse/node')
const Product = require('./../models/product')

exports.get = async () => {
    let query = new Parse.Query('Product')
        query.descending('createdAt')
        query.include(['area', 'teachers'])

    let res = await query.find()

    return res
}

exports.update = async (product, body) => {
    if (body.name) product.set('name', body.name)
    if (body.type) product.set('type', body.type)
    if (body.description) product.set('description', body.description)
    if (body.workload > 0) product.set('workload', body.workload)
    if (body.validate) product.set('validate', body.validate)
    if (body.price) product.set('price', body.price)
    if (body.banner) product.set('banner', body.banner)
    if (body.bannerType) product.set('bannerType', body.bannerType)
    if (body.video) product.set('video', body.video)
    if (body.teachers && body.teachers.length) {
        body.teachers.forEach(teacher => {
            let User = Parse.Object.extend('User')
            let user = new User()
                user.id = teacher.id

            product.addUnique('teachers', user)
        });
    }

    if (body.area) {
        let Area = Parse.Object.extend('Area')
        let area = new Area()
            area.id = body.area

        product.set('area', area)
    }

    if (body.ready === true || body.ready === false) product.set('ready', body.ready)
    if (body.promotional === true || body.promotional === false) product.set('promotional', body.promotional)
    if (body.online === true || body.online === false) product.set('online', body.online)

    let saved = await product.save()

    return saved
}

exports.getById = async (id) => {
    let query = new Parse.Query('Product')
        query.include(['area', 'teachers'])

    let product = await query.get(id)
    
    return product

}

exports.getBySlug = async (slug) => {
    let query = new Parse.Query('Product')
        query.equalTo('slug', slug)

    let product = await query.first()

    return product
}

exports.getArea = async (id) => {
    let query = new Parse.Query('Area')
    
    let area = await query.get(id)

    return area
}

exports.delete = async (product) => {
    let deleted = await product.destroy()

    return deleted
}

exports.create = async (body) => {
    let data = new Product(body.name, body.slug, body.type, body.area, body.description, body.workload, body.validate, body.price, body.banner, body.bannerType, body.video, body.teachers, body.folder, body.discount, body.init)

    let Area = Parse.Object.extend('Area')
    let area = new Area()
        area.id = data.area

    let Produto = Parse.Object.extend('Product')
    let product = new Produto()
        product.set('name', data.name)
        product.set('slug', data.slug)
        product.set('type', data.type)
        product.set('area', area)
        product.set('description', data.description)
        product.set('workload', data.workload)
        product.set('validate', data.validate)
        product.set('price', data.price)
        product.set('banner', data.banner)
        product.set('bannerType', data.bannerType)
        product.set('video', data.video)
        product.set('folder', data.folder)  
        product.set('discount', data.discount)

        if (data.init) {
            product.set('init', new Date(data.init))
        }

        product.set('teachers', data.teachers.map(teacher => {
            let User = Parse.Object.extend('User')
            let user = new User()
                user.id = teacher

            return user
        }))
        product.set('ready', false)
        product.set('promotional', false)
        product.set('online', true)

    let created = await product.save()

    return created
}